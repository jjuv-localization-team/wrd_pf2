#include <iostream>
#include <pcl/point_cloud.h>
#include <Eigen/Dense>
#include <vector>
#include <list>
#include <algorithm>
#include <time.h>

//#include <qtextcode>

using Eigen::MatrixXd;
using namespace std;

const int sample_size=1000;
vector<int> sample_array(sample_size);

const int dim=3;//维数
double global_error[dim]={2000,2000,20};//cm,cm,du;x,y,angle
double grid_scale[dim]={100,100,1};//cm,cm,du
//double min_grid_scale[dim]={10,10,0.1};
int min_grid_count=200;
int iter_cnt=0;
int model_flag=0;//0--SF;1--PF
const int max_space_count=int(pow(2.,double(dim)));

int space_count=1;
int grid_size[dim];
int grid_count=1;
int grids_in_space;
int particle_count=1;
int particle_in_space;
int ratio_gp;

double pos_now[dim]={0,0,0};//当前位置初值=上一次迭代/定位的位置+推算出的相对位置

struct Space
{
    int id;
    double weight;
};
struct Particle
{
    double x,y;
    double angle;
    double weight;
    int number;
};
struct Point
{
    double x,y;
    double angle;
};

const int T=10;
vector<Point> real_point(T);
vector<Point> obv_point(T);
int t;
vector<Space> space(max_space_count);

//vector<Particle> p(10);
//Particle a;
template <typename mytype>
bool compare3(mytype,mytype);
bool compare2(int,int);
bool compareP(Particle,Particle);
bool compareS(Space,Space);

//测试数据初始化（一个运动的点）
void test_init()
{
    //    vector<Point> real_point(5);
    //    vector<Point> obv_point(5);
    // srand(time(NULL));
    for(int i=0;i<real_point.size();i++)
    {
        real_point[i].x=cos(M_PI/8*i)*4000+5000;
        real_point[i].y=sin(M_PI/8*i)*4000+5000;
        real_point[i].angle=M_PI*(1./8*i+0.5);
        obv_point[i].x=real_point[i].x+rand()%(int(global_error[0]/2.));
        obv_point[i].y=real_point[i].y+rand()%(int(global_error[1]/2.));
        obv_point[i].angle=real_point[i].angle+rand()%(int(global_error[2]/2.));
    }
}

//粒子滤波初始化
void pf_init()
{
    //space_count=int(pow(2.0,double(dim)));
    //space_count=1;
    if(space_count==1)
    {
        if(model_flag==0)
        {
            grid_count=1;
            for(int i=0;i<dim;i++)
            {
                grid_size[i]=int(global_error[i]/grid_scale[i]);
                grid_count*=grid_size[i];
                //  particle_count*=grid_size[i]/2;
            }

            if(grid_count>min_grid_count)
            {
                space_count=max_space_count;
            }
            else
            {
                space_count=1;
                model_flag=1;
                //  srand(time(NULL));
                for(int i=0;i<sample_size;i++)
                {
                    sample_array[i]=rand()%grid_count;
                }
            }
            particle_count=grid_count/space_count;

            grids_in_space=grid_count/space_count;
            particle_in_space=ceil(particle_count/space_count);
            ratio_gp=grid_count/particle_count;

            particle_count=particle_in_space*space_count;
            for(int i=0;i<space_count;i++)
            {
                space[i].id=i;
                space[i].weight=0;
            }
        }
        //        if(model_flag==1)
        //        {
        //            particle_count=1000;
        //        }
        //        else
        //        {
        //            model_flag++;
        //        }
    }
}

//vector<int> p_num(particle_count);
//vector<Particle> p(particle_count);

//离散化的粒子随机采样
void getP_num(vector<int> & p_num)
{
    //srand(time(NULL));
    if(model_flag==0)
    {
        for(int j=0;j<space_count;j++)
        {
            for(int i=0;i<particle_in_space;i++)
            {
                p_num[i+j*particle_in_space]=ratio_gp*i+rand()%ratio_gp+space[j].id*grids_in_space;
            }
        }
    }
    if(model_flag==1)
    {
        for(int i=0;i<particle_count;i++)
        {
            p_num[i]=sample_array[rand()%sample_size];
        }
    }

}

//根据样本编号生成对应粒子
void num2pos(const vector<int> & p_num,vector<Particle> & p,const double * pos_now)
{
    int num_prop[p_num.size()][dim];
    double pos_prop[p_num.size()][dim];
    // srand(time(NULL));
    for(int i=0;i<p_num.size();i++)
    {
        int value=p_num[i];
        int s=grid_count;
        for(int j=0;j<dim;j++)
        {
            s=s/grid_size[j];
            num_prop[i][j]=value/s;
            value=value%s;
            pos_prop[i][j]=num_prop[i][j]*grid_scale[j]-global_error[j]/2.+rand()/(RAND_MAX+1.0)*grid_scale[j]*1.5;
        }
    }
    //cout<<p_num.size()<<endl;
    for(int i=0;i<p_num.size();i++)
    {
        p[i].x=pos_prop[i][0]+pos_now[0];
        p[i].y=pos_prop[i][1]+pos_now[1];
        p[i].angle=pos_prop[i][2]+pos_now[2];
        p[i].number=p_num[i];
    }
}
void pos2num(){}

//计算粒子权重/概率（测试版：与距离相关的高斯分布）
void calWeight(vector<Particle> & p)
{
    /*test*/
    for(int i=0;i<p.size();i++)
    {
        //        p[i].weight=1./pow(((p[i].x-obv_point[t].x)*(p[i].x-obv_point[t].x)
        //                        +(p[i].y-obv_point[t].y)*(p[i].y-obv_point[t].y)
        //                        +(p[i].angle-obv_point[t].angle)*(p[i].angle-obv_point[t].angle)*10000),0.5);

        double dis=(abs(p[i].x-real_point[t].x)//);
                    +abs(p[i].y-real_point[t].y)//);
                    +abs(p[i].angle-real_point[t].angle)*100)/3;
        dis=dis/100;
        p[i].weight=exp(-dis*dis/2);
    }
}


//根据粒子权重生成待抽样本集
vector<int> w2sParticle(vector<Particle> & p,int a_size=1000,double ratio=0.4)
{

    vector<int> a(a_size);
    sort(p.begin(),p.end(),compareP);
    int sample_cnt=p.size()*ratio;
    double sum_weight=0;
    for(int i=0;i<sample_cnt;i++)
    {
        sum_weight+=p[i].weight;
    }

    int k=0;
    for(int i=0;i<sample_cnt;i++)
    {
        int j=0;
        int value=int(p[i].weight/sum_weight*a_size);
        for(j=0;j<value;j++)
        {
            if(k<a_size)
                a[k++]=p[i].number;
        }
    }
    if(k<a_size)
    {
        for(int i=k;i<a_size;i++)
        {
            a[i]=p[sample_cnt-1].number;
        }
    }
    return a;
}

//粒子空间重采样（自己增加的部分）
//void resample(vector<int> & p_num,vector<Particle> & p,vector<Space> & space)
void resampleSpace(vector<Space> & space,const vector<Particle> & p,double * pos_now)
{     
    if(space_count>1)
    {
        for(int i=0;i<p.size();i++)
        {
            int space_id=p[i].number/grids_in_space;
            for(int k=0;k<max_space_count;k++)
            {
                if(space[k].id==space_id)
                {
                    space[k].weight+=p[i].weight;
                    break;
                }
            }
        }
        sort(space.begin(),space.end(),compareS);
        space_count=space_count/2;
    }
    if(space_count==1)
    {
        int value=space[0].id;
        int s=int(pow(2.,double(dim)));
        int k[dim];
        for(int i=0;i<dim;i++)
        {
            global_error[i]/=2.;
            //计算新的全局坐标
            s=s/2;
            k[i]=value/s;
            value=value%s;
            pos_now[i]+=-global_error[i]/2.+k[i]*global_error[i];
        }
    }
}





int main(int argc, char *argv[])
{
    //    //    p[0](12.1,23.3,90.,0.6);
    //    p[0]={12.1,23.3,90.,0.6};
    //    p[1]={20,2,45,0.2};
    //    //  a(1,2,45,0.2);
    //    p[3].x=0.4;
    //    p[3].weight=0.43;
    //    //p[3][2]=2.4;
    //    int a[3]={3,2,6};
    //    sort(a,a+2,compare2);
    //    sort(p.begin(),p.begin()+4,compareP);
    //    for(int i=0;i<4;i++)
    //    {
    //        cout<<p[i].x<<endl;
    //    }
    //    cout << "Hello World!" << endl;
    //    timeval start, end;
    //    get(&start, null);
    double start_t=clock();
    srand(time(NULL));
    test_init();
    for(t=0;t<T;t++)
    {
        if(t==0)
        {
            pos_now[0]=obv_point[t].x;
            pos_now[1]=obv_point[t].y;
            pos_now[2]=obv_point[t].angle;
        }
        else
        {
            pos_now[0]+=real_point[t].x-real_point[t-1].x;
            pos_now[1]+=real_point[t].y-real_point[t-1].y;
            pos_now[2]+=real_point[t].angle-real_point[t-1].angle;
        }


        pf_init();
        cout<<"pf_init OK"<<endl;
        vector<int> p_num(particle_count);
        vector<Particle> p(particle_count);
        getP_num(p_num);
        cout<<"getP_num OK"<<endl;
        num2pos(p_num,p,pos_now);
        cout<<"num2pos OK"<<endl;
        calWeight(p);
        cout<<"calWeight OK"<<endl;

        if(model_flag==0)
        {
            resampleSpace(space,p,pos_now);
            cout<<"space[0].id="<<space[0].id<<endl;
        }
        if(model_flag==1)
        {
            sample_array=w2sParticle(p);
            cout<<"particle[0].x="<<p[0].x<<endl;
        }
        cout<<"real_point.x="<<real_point[t].x<<endl;
    }
    double end_t=clock();
    double run_t=end_t-start_t;
    //    gettimeofday(&end, null);
    //    double run_t=1000*(end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec)/1000;
    cout<<"elapsed time"<<run_t<<endl;
    return 0;
}

template <typename mytype>
bool compare3(mytype a,mytype b)
{
    if(typeid(a)==typeid(Particle))
    {
        return a.weight>=b.weight;
    }
    return a>b;
}
bool compare2(int a,int b)
{
    return a>b;
}
bool compareP(Particle a,Particle b)
{
    return a.weight>b.weight;//按降序排,带"="可能会导致报错
}
bool compareS(Space a,Space b)
{
    return a.weight>b.weight;//按降序排
}



